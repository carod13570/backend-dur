var express = require('express')
var app = express()
const cors = require('cors');

// middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use(cors());

const messages = [{
        id: 1,
        from: "firstperson@mail.fr",
        subject: "URGENT",
        body: "Faut faire ça",
        read: false,
    },{
        id: 2,
        from: "secondperson@mail.fr",
        subject: "Promo",
        body: "Sur tous les livres1",
        read: true,
    },{
        id: 3,
        from: "troisiemeperson@mail.fr",
        subject: "Promo",
        body: "Sur tous les livres2",
        read: true,
    },{
        id: 4,
        from: "quatreperson@mail.fr",
        subject: "Promo",
        body: "Sur tous les livres3",
        read: true,
    }
]

app.use((req,res,next)=>{
    res.setHeader('Access-Control-Allow-Origin','*')
    next();
})

app.get('/messages',function(req,res) {
    res.send(messages)
})

app.post('/createMessage', function(req, res){
    // recupère la data envoyé dans le body
    let data = req.body;

    messages.push(data);
    res.send(messages);
})

app.listen(8899)